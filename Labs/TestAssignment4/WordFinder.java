import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class WordFinder {
	private static Map<String, Integer> finalOutput = new HashMap();
	private static AnagramDictionary dict;
	public static void main(String[] args) throws FileNotFoundException {
		boolean status = true;
		try{
			 dict = new AnagramDictionary(args[0]);
			}
			catch(FileNotFoundException e)
			{
				System.out.println("Exception found and file is not found in the specified path ");
				System.out.println(e.toString());
				status=false;
			}
		
		
		while (status) {
			 String input = null;
			System.out.println("Type . to quit ");
			System.out.println("Rack?");
			Scanner in = new Scanner(System.in);
			if(in.hasNext())
			{
				input= in.nextLine();
			
			}
			else{
				break;
			}
			
			if (input.equals(".")) {
				status = false;
				break;
			}
			
			
			final ScoreTable table = new ScoreTable();
			Rack rack = new Rack();
     
			ArrayList<String> n = rack.findSubset(input);
			ArrayList<String> anagrams = new ArrayList();

			for (String s : n) {
				if (!(dict.getAnagramsOf(s) == null))
					anagrams.addAll(dict.getAnagramsOf(s));
			}

			for (String s : anagrams)
				finalOutput.put(s, table.getScore(s));

			Collections.sort(anagrams, new Comparator<String>() {
				@Override
				public int compare(String s1, String s2) {
					int score1 = finalOutput.get(s1);
					int score2 = finalOutput.get(s2);
					if (score1 == score2)
						return s1.compareTo(s2);
					else
						return score2 - score1;
				}
			});

			for (String s : anagrams)
				System.out.println("Score: " + finalOutput.get(s) + " String = " + s);

		}

	}

}
