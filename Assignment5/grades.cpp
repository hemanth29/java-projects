// Name:Hemanth Eshwarappa
// USC NetID: eshwarap
// CSCI 455 PA5
// Fall 2017

/*
 * grades.cpp
 * A program to test the Table class.
 * How to run it:
 *      grades [hashSize]
 *
 * the optional argument hashSize is the size of hash table to use.
 * if it's not given, the program uses default size (Table::HASH_SIZE)
 *
 */

#include "Table.h"

// cstdlib needed for call to atoi
#include <cstdlib>

int main(int argc, char * argv[]) {

  // gets the hash table size from the command line

  int hashSize = Table::HASH_SIZE;

  Table * grades;  // Table is dynamically allocated below, so we can call
                   // different constructors depending on input from the user.

  if (argc > 1) {
    hashSize = atoi(argv[1]);  // atoi converts c-string to int

    if (hashSize < 1) {
      cout << "Command line argument (hashSize) must be a positive number"
	   << endl;
      return 1;
    }

    grades = new Table(hashSize);

  }
  else {   // no command line args given -- use default table size
    grades = new Table();
  }


  grades->hashStats(cout);

  // add more code here
  // Reminder: use -> when calling Table methods, since grades is type Table*
  int checker=0;
  while(!checker){
//temp variables
  string userCommand;
  string name;
  int score;


  cout<<"Enter a command"<<endl;
  cin>>userCommand;

//check for user input
    if(userCommand=="insert")
     {
          cin>>name;
          cin>>score;
          grades->insert(name,score);

     }
   else if(userCommand=="change")
     {
          cin>>name;
          cin>>score;

     }

   else if(userCommand=="lookup")
    {
          cin>>name;
          grades->lookup(name);

    }

    if(userCommand=="remove")
    {
        cin>>name;
        grades->remove(name);

    }

   else if(userCommand=="print")
    {
        grades->printAll();

    }

    else if(userCommand=="size")
    {
        grades->numEntries();

    }
    else if(userCommand=="stats")
    {
       grades->hashStats(cout);

    }
   else if(userCommand=="help")
    {
        cout<<"Commands available are "<<endl;
        cout<< "insert name score, change name score, lookup name, remove name, print, size, stats, help, quit "<<endl;

    }
   else if(userCommand=="quit")
    {
        checker =1;
        return 0;
    }


  }





  return 0;
}
