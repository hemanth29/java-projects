// Name:Hemanth Eshwarappa
// USC NetID:eshwarap
// CSCI 455 PA5
// Fall 2017

// Table.cpp  Table class implementation


/*
 * Modified 11/22/11 by CMB
 *   changed name of constructor formal parameter to match .h file
 */

#include "Table.h"

#include <iostream>
#include <string>
#include <cassert>


// listFuncs.h has the definition of Node and its methods.  -- when
// you complete it it will also have the function prototypes for your
// list functions.  With this #include, you can use Node type (and
// Node*, and ListType), and call those list functions from inside
// your Table methods, below.

#include "listFuncs.h"


//*************************************************************************


Table::Table() {

	hashSize = HASH_SIZE;
	data =  new ListType[hashSize];
	for (int i = 0; i < HASH_SIZE;i++){
		data[i]=NULL;
	}
}


Table::Table(unsigned int hSize) {

	hashSize = hSize;
	data = new ListType[hashSize];
	for (int i = 0; i < hashSize;i++){
		data[i]=NULL;
	}
}

//lookup for a value of key in the hashtable
int * Table::lookup(const string &key) {
   int searchkey = hashCode(key);
	return searchValue(data[searchkey],key);  // dummy return value for stub
}
//remove a key from table
bool Table::remove(const string &key) {
    int removekey  = hashCode(key);
	return deletefromList(data[removekey],key);
}
//insert a val to the hashtable
bool Table::insert(const string &key, int value) {
     int insertkey = hashCode(key);
	if(searchKey(data[insertkey],key)==true){
		return false;
	}
	inserti(data[insertkey],key,value);
	return true;
}
//calculate no of entries in tavle
int Table::numEntries() const {

    int  countall = 0;
	for (int i = 0; i < hashSize; i++){

    Node *tempVar = data[i];
	int returnsz = 0;

	while(tempVar != NULL){
		returnsz++;
		tempVar  = tempVar->next;
	}
	countall += returnsz;
	}

  return countall;    // dummy return value for stub
}

//print all the values in the table
void Table::printAll() const {

	for( int i = 0; i < hashSize; i++){
		   printallval(data[i]);
		   cout<<endl;
	}

}
//stats for the table
void Table::hashStats(ostream &out) const {



	int full = 0;
	int maxchain = 0;
	for ( int i = 0; i < hashSize; i++){
		if(data[i] != NULL){
			full++;
			}

    Node *tempVar = data[i];
	int returnsz = 0;

	while(tempVar != NULL){
		returnsz++;
		tempVar  = tempVar->next;
	}

	 int result = returnsz;

		if ( (result >= maxchain) && (data[i] != NULL )){
				maxchain = result;
	}
	}
	out << "Bucket Nos = : " << hashSize << endl;
	out << "Entry Nos = : " << numEntries()<< endl;
	out << "number of non-empty buckets: " << full << endl;
	out << "longest chain: " << maxchain << endl;

}



// add definitions for your private methods here
