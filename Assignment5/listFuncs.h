// Name:Hemanth Eshwarappa
// USC NetID: eshwarap
// CSCI 455 PA5
// Fall 2017


//*************************************************************************
// Node class definition
// and declarations for functions on ListType

// Note: we don't need Node in Table.h
// because it's used by the Table class; not by any Table client code.


#ifndef LIST_FUNCS_H
#define LIST_FUNCS_H

using namespace std;

struct Node {
  string key;
  int value;

  Node *next;

  Node(const string &theKey, int theValue);

  Node(const string &theKey, int theValue, Node *n);
};


typedef Node * ListType;

//*************************************************************************
//add function headers (aka, function prototypes) for your functions
//that operate on a list here (i.e., each includes a parameter of type
//ListType or ListType&).  No function definitions go in this file.



// add new node
bool inserti(ListType &node,const string &key,int val);


// deleting from the list
bool deletefromList(ListType &node,const string &key);

//search for the key
bool searchKey(ListType &node,const string &key);


// search for the value
int* searchValue(ListType &node,const string &key);


// Prints the elements in list
void printallval(ListType &node);








// keep the following line at the end of the file
#endif
