// Name:Hemanth Eshwarappa
// USC NetID:eshwarap
// CSCI 455 PA5
// Fall 2017


#include <iostream>

#include <cassert>

#include "listFuncs.h"

using namespace std;

Node::Node(const string &theKey, int theValue) {
  key = theKey;
  value = theValue;
  next = NULL;
}

Node::Node(const string &theKey, int theValue, Node *n) {
  key = theKey;
  value = theValue;
  next = n;
}




//*************************************************************************
// put the function definitions for your list functions below

//insert to a linked list using first constructor
bool inserti(ListType &node,const string &key, int val){
	node = new Node(key, val,node);
	return true;
}


//traverse each node and print it from the linked list
void printallval(ListType &node){

	Node *newNode=node;

	if (newNode == NULL){
		return;
	}
	while (newNode != NULL){
		cout << newNode->key << " " << newNode->value<<"  ";
		newNode = newNode->next;
	}
}
//search for a key in linked list
bool searchKey(ListType &node,const string &key){
	Node *newNode = node;

	if (newNode == NULL){
		return false;
	}
	while(newNode != NULL){
		if (newNode->key == key){
			return true;
		}
		newNode = newNode->next;
	}
	return false;
}
//search for a value in linked list
int* searchValue(ListType &node,const string &key){
	Node *newNode;
	newNode  = node;
	if (newNode == NULL){
		return NULL;
	}
	while(newNode != NULL){
		if (newNode->key == key){
			return &(newNode->value);
		}
		newNode = newNode->next;
	}
	return NULL;

}
//delete from a linked list
bool deletefromList(ListType &node,const string &key){
	Node *newNode;
	newNode  = node;
	Node *temp;
	if (newNode == NULL){
		return false;
	}
	if(newNode->key == key){
		Node *torem = node;
	    node = node->next;
	    delete torem;
		return true;
	}
	while(newNode->next != NULL){
		if (newNode->next->key == key){
			temp = newNode->next;
			newNode->next = newNode->next->next;
			delete temp;
			return true;
		}
		newNode = newNode->next;
	}
	return false;
}
