
// Name:Hemanth Eshwarappa
// USC NetID:eshwarap
// CSCI455 PA2
// Fall 2017

import java.util.ArrayList;

/*
   class SolitaireBoard
   The board for Bulgarian Solitaire.  You can change what the total number of cards is for the game
   by changing NUM_FINAL_PILES, below.  Don't change CARD_TOTAL directly, because there are only some values
   for CARD_TOTAL that result in a game that terminates.
   (See comments below next to named constant declarations for more details on this.)
 */

public class SolitaireBoard {

	public static final int NUM_FINAL_PILES = 9;
	// number of piles in a final configuration
	// (note: if NUM_FINAL_PILES is 9, then CARD_TOTAL below will be 45)

	public static final int CARD_TOTAL = NUM_FINAL_PILES * (NUM_FINAL_PILES + 1) / 2;
	// bulgarian solitaire only terminates if CARD_TOTAL is a triangular number.
	// see: http://en.wikipedia.org/wiki/Bulgarian_solitaire for more details
	// the above formula is the closed form for 1 + 2 + 3 + . . . +
	// NUM_FINAL_PILES

	// Note to students: you may not use an ArrayList -- see assgt description
	// for details.

	/**
	 * Representation invariant: The Arraylist piles is the list which is passed
	 * to Constructor with parameters. piles values should all add up to the
	 * constant CARD_TOTAL piles[0]+...+piles[n]= CARD_TOTAL piles is a list of
	 * integers. piles should all be positive integers piles[i]>0 where i=0 to
	 * n.
	 * 
	 */

	// <add instance variables here>

	// pilesValues array will save the piles value either provided by user or
	// generated randomly
	private int[] pilesValues = new int[NUM_FINAL_PILES + 3];
	// arrayElementLoc is to keep track of the partially filled array while
	// execution.It will point to 0 initially.
	private int arrayElementLoc = 0;

	/**
	 * Creates a solitaire board with the configuration specified in piles.
	 * piles has the number of cards in the first pile, then the number of cards
	 * in the second pile, etc. PRE: piles contains a sequence of positive
	 * numbers that sum to SolitaireBoard.CARD_TOTAL
	 */
	public SolitaireBoard(ArrayList<Integer> piles) {

		for (int i = 0; i < piles.size(); i++) {
			pilesValues[i] = piles.get(i); // store each of the piles given by
											// user in our pilesValues array.
		}
		arrayElementLoc = piles.size(); // arrayElementLoc is now pointing to last element
										
		assert isValidSolitaireBoard(); // assert statement to check if config is correct
										
	}

	/**
	 * Creates a solitaire board with a random initial configuration.
	 */
	public SolitaireBoard() {
		int temp_i = 0; // temporary variable to keep track of the elements in
						// array and save it to arrayElementLoc
		int temp_Card_Total = CARD_TOTAL;
        
		while (temp_Card_Total > 0 && temp_i <= NUM_FINAL_PILES) {
			int randomNumber = (int) (Math.random() * temp_Card_Total) + 1; 
			// Generate a random number based on card_total
												
			pilesValues[temp_i] = randomNumber;
			temp_Card_Total -= randomNumber; // subtract the number generated
												// from CARD_TOTAL(copy of it)
			temp_i++;
		}
        
		arrayElementLoc = temp_i;
		assert isValidSolitaireBoard();  // test the board config
	}

	/**
	 * Plays one round of Bulgarian solitaire. Updates the configuration
	 * according to the rules of Bulgarian solitaire: Takes one card from each
	 * pile, and puts them all together in a new pile. The old piles that are
	 * left will be in the same relative order as before, and the new pile will
	 * be at the end.
	 */
	public void playRound() { 
		// variables local to playRound Function
		int temp_ArrayElementLoc = arrayElementLoc; 
		// A temp variable to be used in comparision to check for piles values
													
		int singleArrayCount = 0; // A variable to fill the array values after
									// changing their initial configuration

		for (int rounds = 0; rounds < temp_ArrayElementLoc; rounds++) {
			int newPile = pilesValues[rounds]; // save the value of each pile in
												// a temp variable.
			newPile--; // reduce one from the value of that pile.

			if (newPile > 0) {
				pilesValues[singleArrayCount++] = newPile;
			} // add it to the array back if its greater than 0
			else {
				arrayElementLoc--; // if the value if not >0 then, point the
									// arrayElementLoc to one behind.
			}

		}

		pilesValues[arrayElementLoc] = temp_ArrayElementLoc; 
		// Add the subtracted sum of all piles in the last element of the array
																
		arrayElementLoc++; // change the pointer variable for partial array to
							// next.

		assert isValidSolitaireBoard();
	}

	/**
	 * Returns true iff the current board is at the end of the game. That is,
	 * there are NUM_FINAL_PILES piles that are of sizes 1, 2, 3, . . . ,
	 * NUM_FINAL_PILES, in any order.
	 */

	public boolean isDone() {
		boolean[] visited = new boolean[CARD_TOTAL]; 
		// Create an array to save the visited status of a number (Check Duplicates)
																
		for (int i = 1; i <= CARD_TOTAL-1 ; i++)
			visited[i] = false; // Set the initial values to false for all
								// values.

		if (arrayElementLoc != NUM_FINAL_PILES) // Return false for lesser array
												// length
		{
			return false;
		}

		else {
			for (int i = 0; i < NUM_FINAL_PILES; i++) {
				//System.out.println("piles value "+pilesValues[i]);
				//If any value is greater than largest possible or the element is a duplicate
				if (visited[pilesValues[i]] == true) 
				{
					return false;
				} 

				else {
					visited[pilesValues[i]] = true; // If no duplicate, set it to visited.
				} 
			}
			assert isValidSolitaireBoard();
			return true;
		}

	}

	/**
	 * Returns current board configuration as a string with the format of a
	 * space-separated list of numbers with no leading or trailing spaces. The
	 * numbers represent the number of cards in each non-empty pile.
	 */
	public String configString() {
		String config = ""; // local variable to save the string
		for (int i = 0; i < arrayElementLoc - 1; i++) {
			config += pilesValues[i] + " ";
		}

		config = config + pilesValues[arrayElementLoc - 1];
		assert isValidSolitaireBoard();
		return config;
	}

	/**
	 * Returns true iff the solitaire board data is in a valid state (See
	 * representation invariant comment for more details.)
	 */
	private boolean isValidSolitaireBoard() {
		int count = 0; // temp variable to save the array values

		for (int i = 0; i < arrayElementLoc; i++) {
			count += pilesValues[i];
		}

		if (count == CARD_TOTAL) // If the array values add up to CARD_TOTAL
									// then its valid
		{
			return true;
		} else {
			return false;
		}
	}

	// <add any additional private methods here>

}