import java.util.ArrayList;
import java.util.Scanner;

// Name:Hemanth Eshwarappa
// USC NetID:eshwarap
// CSCI455 PA2
// Fall 2017

/**
 * The class takes two arguments -u for user input and -s for single step play
 * If user input is not chosen it starts with a random board config. If
 * singlestep is not chosen then it runs till the final config (piles values in
 * unsorted order or sorted order)
 * 
 */

public class BulgarianSolitaireSimulator { // ArrayList to save the integer
											// values after splitting and
											// validating line from user
	static ArrayList<Integer> numbersList = null;

	public static void main(String[] args) {

		boolean singleStep = false;
		boolean userConfig = false;

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-u")) {
				userConfig = true;
			} else if (args[i].equals("-s")) {
				singleStep = true;
			}
		}
		// checks the validity of user input
		checkUserInput(singleStep, userConfig);

		// checks the command line arguments and run the Solitaire Board
		// accordingly
		checkUserConfig(userConfig, singleStep, numbersList);
	}

	// <add private static methods here>
	// Checks user input and validates for exceptions and negative numbers and
	// also count of piles must be 45
	private static void checkUserInput(boolean singleStep, boolean userConfig) {
		if (userConfig == true) {
			boolean isInputValid = false;
			while (!isInputValid) {
				// <add code here>
				instructUser();
				Scanner in = new Scanner(System.in);
				String inputString = in.nextLine();
				String[] numbers = inputString.split(" ");

				numbersList = new ArrayList<Integer>();
				int countOfPiles = 0;
				boolean isCompleted = true;
				for (String str : numbers) {
					try {
						int tempParsedInt = Integer.parseInt(str);
						if (tempParsedInt <= 0) // checking for negative
												// integers
						{
							System.out.println(
									"ERROR: Each pile must be a non negative integer and total of all piles must be 45");
							isCompleted = false;
							break;
						}
						countOfPiles += tempParsedInt;
						numbersList.add(tempParsedInt);
					} catch (NumberFormatException e) { // catching the
														// exceptions in general
						isInputValid = false;
						System.out.println(
								"ERROR:Please enter a valid number in each pile and total of all piles must be 45");
					}
				}

				isInputValid = checkCountOfPiles(isCompleted, countOfPiles);
			}

		}

	}

	// check the count of all the piles
	private static boolean checkCountOfPiles(boolean isCompleted, int countOfPiles) {
		if (isCompleted && countOfPiles == 45) {
			return true;
		} else {
			System.out.println("ERROR: Each pile must have at least one card and the total number of cards must be 45");
			return false;
		}

	}

	// instructs the user to enter input
	private static void instructUser() {
		System.out.println("Number of total cards is 45");
		System.out
				.println("You will be entering the initial configuration of the cards (i.e., how many in each pile).");
		System.out.println("Please enter a space-seperated list of positive integers followed by newline");

	}

	// creates the board according to given configuration
	private static void createBoard(SolitaireBoard board, boolean singleStep, boolean userConfig,
			ArrayList<Integer> numbersList) {

		if (singleStep == true)
			runEachRound(board, true);
		else
			runEachRound(board, false);

	}

	// checks the usesr confif argument and calls create board accordingly
	private static void checkUserConfig(boolean userConfig, boolean singleStep, ArrayList<Integer> numbersList) {
		if (userConfig == false) {
			instructUser();
			SolitaireBoard board = new SolitaireBoard();
			String s = board.configString();

			createBoard(board, singleStep, userConfig, null);

		} else {
			SolitaireBoard board = new SolitaireBoard(numbersList);
			String s = board.configString();
			System.out.println(s);
			createBoard(board, singleStep, userConfig, null);

		}

	}

	// Checks whether to run till the end at once or wait for user to press
	// enter
	private static void runEachRound(SolitaireBoard board, boolean val) {
		while (board.isDone() == false) {
			board.playRound();
			String temp = board.configString();
			System.out.println("Current Configuration: " + temp);
			if (val == true && board.isDone() == false) {
				pressEnter();
			}
		}

	}

	// checks until user press enter to go to next round
	public static void pressEnter() {
		System.out.println("<Type return to continue>");
		Scanner scanner = new Scanner(System.in);
		scanner.nextLine();

	}

}