
// Name: Hemanth Eshwarappa
// USC NetID: eshwarap
// CS 455 PA4
// Fall 2017

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * A dictionary of all anagram sets. Note: the processing is case-sensitive; so
 * if the dictionary has all lower case words, you will likely want any string
 * you test to have all lower case letters too, and likewise if the dictionary
 * words are all upper case.
 */

public class AnagramDictionary {

	/**
	 * Create an anagram dictionary from the list of words given in the file
	 * indicated by fileName. PRE: The strings in the file are unique.
	 * 
	 * @param fileName
	 *            the name of the file to read from
	 * @throws FileNotFoundException
	 *             if the file is not found
	 */
    //A map to save all the possible anagrams within the dictionary itself
	private Map<String, ArrayList> mapAnagram = new HashMap();
	//ArrayList to be passed for second argument of Map
	private ArrayList<String> list;

	public AnagramDictionary(String fileName) throws FileNotFoundException {
        //Take input from file
		Scanner sc = new Scanner(new File(fileName));
		//Until we have next word run while
		while (sc.hasNext()) {
			String s = sc.next();
			if (s.trim().isEmpty()) {
				continue;
			}
        //sort the string to check if its already present in the list of the map
			String temp = sortString(s);

			if (!mapAnagram.containsKey(temp)) {
				list = new ArrayList();
				//As the word is not present just add the originial word to the arraylist
				//key-sorted string values=Arraylist of all words which give same sorted string
				list.add(s);
				mapAnagram.put(temp, list);

			}

			else {
				//retrieve the present list
				list = mapAnagram.get(temp);
				//append the new word to the list
				list.add(s);
				mapAnagram.put(temp, list);
			}

		}
		sc.close();
		// System.out.println(mapAnagram.toString());
	}

	/**
	 * Get all anagrams of the given string. This method is case-sensitive. E.g.
	 * "CARE" and "race" would not be recognized as anagrams.
	 * 
	 * @param s
	 *            string to process
	 * @return a list of the anagrams of s
	 * 
	 */
//To get the anagrams of a word from main function
	public ArrayList<String> getAnagramsOf(String s) {
		//As we already know the anagrams within the dictionary, it only compares it with the map key and returns values
		String temp = sortString(s);
        if(mapAnagram.get(temp)==null)
        {
        	return null;
        }
		return mapAnagram.get(temp); // DUMMY CODE TO GET IT TO COMPILE
	}
   //A function to sort a given string.
	private static String sortString(String string) {

		// convert input string to Character array
		char[] charArray = string.toCharArray();

		// Sort, ignoring case during sorting
		Arrays.sort(charArray);
		String sortedString = new String(charArray);

		return sortedString;

	}

}
