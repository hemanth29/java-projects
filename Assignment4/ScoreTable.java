
// Name: Hemanth Eshwarappa
// USC NetID: eshwarap
// CS 455 PA4
// Fall 2017

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ScoreTable {

	/*
	 * (1 point)-A, E, I, O, U, L, N, S, T, R (2 points)-D, G (3 points)-B, C,
	 * M, P (4 points)-F, H, V, W, Y (5 points)-K (8 points)- J, X (10
	 * points)-Q, Z
	 */

	static Map<Character, Integer> alphabets = new HashMap();

	ScoreTable() {
		addHardCodedValues();
	}

	// Adding values to all the characters in english Alphabet
	private static void addHardCodedValues() {
		// Hard code all the values of alphabets to each character.
		alphabets.put('a', 1);
		alphabets.put('e', 1);
		alphabets.put('i', 1);
		alphabets.put('o', 1);
		alphabets.put('u', 1);
		alphabets.put('l', 1);
		alphabets.put('n', 1);
		alphabets.put('s', 1);
		alphabets.put('t', 1);
		alphabets.put('r', 1);

		alphabets.put('d', 2);
		alphabets.put('g', 2);

		alphabets.put('b', 3);
		alphabets.put('c', 3);
		alphabets.put('m', 3);
		alphabets.put('p', 3);

		alphabets.put('f', 4);
		alphabets.put('h', 4);
		alphabets.put('v', 4);
		alphabets.put('w', 4);
		alphabets.put('y', 4);

		alphabets.put('k', 5);

		alphabets.put('j', 8);
		alphabets.put('x', 8);

		alphabets.put('q', 10);
		alphabets.put('z', 10);
	}

	public static int getScore(String string) {
		// while getting the score of each string, first parse it into
		// characters and then calculate the score.
		String temp = string.toLowerCase();
		// we are converting all caps to small letters because score doesnt
		// change for any of these
		char[] charArray = temp.toCharArray();
		int count = 0;

		for (int i = 0; i < charArray.length; i++) {
			count += alphabets.get(charArray[i]);
		}
		return count;

	}

}
