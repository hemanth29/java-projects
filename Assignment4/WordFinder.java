
// Name: Hemanth Eshwarappa
// USC NetID: eshwarap
// CS 455 PA4
// Fall 2017

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class WordFinder {
	private static Map<String, Integer> finalOutput = new HashMap();
	private static AnagramDictionary dict;
	private static ArrayList<String> n;
	private static ArrayList<String> anagrams;

	public static void main(String[] args) throws FileNotFoundException {
		boolean status = true;
		if (args.length == 0)
			dict = new AnagramDictionary("sowpods.txt");
		else {
			try {
				dict = new AnagramDictionary(args[0]);
			} catch (FileNotFoundException e) {
				System.out.println("Exception found and file is not found in the specified path ");
				System.out.println(e.toString());
				status = false;
			}
		}
		// Run infinitely until user press . for different words of the rack.
		while (status) {
			String input = null;
			System.out.println("Type . to quit ");
			System.out.println("Rack?");
			Scanner in = new Scanner(System.in);
			if (in.hasNext()) {
				input = in.nextLine();

			} else {
				break;
			}

			if (input.equals(".")) {
				status = false;
				break;
			}

			// Calls a constructor of ScoreTable
			final ScoreTable table = new ScoreTable();
			// Calls the constructor of Rack
			Rack rack = new Rack();

			// n will save the arraylist of
			n = rack.findSubset(input);
			anagrams = new ArrayList();

			for (String s : n) {
				if (!(dict.getAnagramsOf(s) == null))
					// Add all the anagrams of all the strings returned from
					// rack
					anagrams.addAll(dict.getAnagramsOf(s));
			}

			// for all the anagrams add it to finaloutput map
			for (String s : anagrams)
				finalOutput.put(s, table.getScore(s));
			// sort based on scores of all the strings and if they are equal
			// then on alphabetical order
			Collections.sort(anagrams, new Comparator<String>() {
				@Override
				// compare using the comparator function
				public int compare(String s1, String s2) {
					int score1 = finalOutput.get(s1);
					int score2 = finalOutput.get(s2);
					if (score1 == score2)
						return s1.compareTo(s2);
					else
						return score2 - score1;
				}
			});
			// Print the output
			for (String s : anagrams)
				System.out.println("Score: " + finalOutput.get(s) + " String = " + s);

		}

	}

}
