// Name:Hemanth Eshwarappa
// USC NetID: 3185152842
// CS 455 PA1
// Fall 2017

import java.util.Random;

/**
 * class CoinTossSimulator
 * 
 * Simulates trials of tossing two coins and allows the user to access the
 * cumulative results.
 * 
 * NOTE: we have provided the public interface for this class.  Do not change
 * the public interface.  You can add private instance variables, constants, 
 * and private methods to the class.  You will also be completing the 
 * implementation of the methods given. 
 * 
 * Invariant: getNumTrials() = getTwoHeads() + getTwoTails() + getHeadTails()
 * 
 */
public class CoinTossSimulator {

  /** These are the variables required to store the values of results obtained from Random generator*/
   private int heads,tails,headtails;
  
   private Random Generator = new Random();
   /**
      Creates a coin toss simulator with no trials done yet.
   */
  public CoinTossSimulator(){
	  Initialize();
  }
   /** This is a overloaded constructor so that we can pass the value directly through an object instead of calling 
    * the run function  */
   public CoinTossSimulator(int trials) {
	   Initialize();
	   run(trials);
	}


   private void Initialize() {
	   heads=0; 
	   tails=0;
	   headtails=0;
	
}


/**
      Runs the simulation for numTrials more trials. Multiple calls to this method
      without a reset() between them *add* these trials to the current simulation.
      
      @param numTrials  number of trials to for simulation; must be >= 1
    */
   public void run(int numTrials) {
	  
	   while(numTrials>0)
	   {   Toss(); 
		   numTrials--;
	   }
	   
   }

 /** The Toss function is called from run function to create two instances of random object and then get the output
  * saved in three variables*/
   private void Toss() {
	int x=Generator.nextInt(2);
	int y=Generator.nextInt(2);
	
	switch(x+y)
	{
	
	case 0: tails++;break;
	case 1: headtails++; break;
	case 2: heads++;break;
	}
	
}

/** The function is just to display the output in the command prompt */
public void display() {
   System.out.println("Number of trials [Result]:" + getNumTrials());
   System.out.println("Two head tosses: "+ getTwoHeads());
   System.out.println("Two tail tosses: "+ getTwoTails());
   System.out.println("one-head one-tail tosses: "+ getHeadTails());
   
}


/**
      Get number of trials performed since last reset.
   */
   public int getNumTrials() {
      
	   return getTwoHeads()+getTwoTails()+getHeadTails(); // DUMMY CODE TO GET IT TO COMPILE
   }


   /**
      Get number of trials that came up two heads since last reset.
   */
   public int getTwoHeads() {
       return heads; // DUMMY CODE TO GET IT TO COMPILE
   }


   /**
     Get number of trials that came up two tails since last reset.
   */  
   public int getTwoTails() {
       return tails; // DUMMY CODE TO GET IT TO COMPILE
   }


   /**
     Get number of trials that came up one head and one tail since last reset.
   */
   public int getHeadTails() {
       return headtails; // DUMMY CODE TO GET IT TO COMPILE
   }


   /**
      Resets the simulation, so that subsequent runs start from 0 trials done.
    */
   public void reset()
    {
	   Initialize();
     }

}