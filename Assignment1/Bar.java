// Name:Hemanth Eshwarappa
// USC NetID: 3185152842
// CS 455 PA1
// Fall 2017

// we included the import statements for you
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;





/**
 * Bar class
 * A labeled bar that can serve as a single bar in a bar graph.
 * The text for the label is centered under the bar.
 * 
 * NOTE: we have provided the public interface for this class. Do not change
 * the public interface. You can add private instance variables, constants,
 * and private methods to the class. You will also be completing the
 * implementation of the methods given.
 * 
 */
public class Bar {
   
	public static final int BOTTOM_BUFFER=44;
	public static final int BOTTOM_STRING_BUFFER=30;
   /**
      Creates a labeled bar.  You give the height of the bar in application
      units (e.g., population of a particular state), and then a scale for how
      tall to display it on the screen (parameter scale). 
  
      @param bottom  location of the bottom of the label
      @param left  location of the left side of the bar
      @param width  width of the bar (in pixels)
      @param barHeight  height of the bar in application units
      @param scale  how many pixels per application unit
      @param color  the color of the bar
      @param label  the label at the bottom of the bar
   */
	private  int bottom_loc,left_loc,width_bar,bar_height;
	private double scale_bar;
	private Color color_bar;
	private String label_bar;
	 
	
	
   public Bar(int bottom, int left, int width, int barHeight,
              double scale, Color color, String label) {
   setTheValues(bottom,left,width,barHeight,scale,color,label);
   }
   
   private void setTheValues(int bottom, int left, int width, int barHeight, double scale, Color color, String label) {
	
	   bottom_loc=bottom;
	   left_loc=left;
	   width_bar=width;
	   bar_height=barHeight;
	   scale_bar=scale;
	   color_bar=color;
	   label_bar=label;
	   
	
}

/**
      Draw the labeled bar. 
      @param g2  the graphics context
   */
   public void draw(Graphics2D g2) {
      g2.setColor(color_bar);
      
	 
	  Rectangle toss_bar = new Rectangle(left_loc,(int)(bottom_loc-BOTTOM_BUFFER-(bar_height/scale_bar)), width_bar, (int)((double)(bar_height)/scale_bar));
	  
	   g2.draw(toss_bar);
	   g2.fill(toss_bar);
	  
	   
	   g2.setColor(Color.black);
	//System.out.println(left_loc);
	   g2.drawString(label_bar, left_loc,bottom_loc-BOTTOM_STRING_BUFFER);
	   
	  
   }
}