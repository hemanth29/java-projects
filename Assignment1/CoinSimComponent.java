// Name:Hemanth Eshwarappa
// USC NetID: 3185152842
// CS 455 PA1
// Fall 2017

import java.awt.Color;

import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;



public class CoinSimComponent extends JComponent {
   /** Constants used in the program.. 
    * 1.For adjusting the screen buffer below
    * 2.For getting the width division from the screen
    * 3.To set a constant width initially*/
	public static final int ADJUST_FACTOR = 75; 
	public static final int WIDTH_DIVIDER = 7;  
	public static final int SET_WIDTH=100;  
	
	/** The variables are declared to pass it to bar object*/
  private int bottom,left,width;
  
  
  /** The scale variable is dependent on the screen height*/
  private double scale; 
  
  /** These variables are to obtain the results from CoinTossSimulator object*/
  private int heads,tails,headtails,trialnum;
  
  /** These variables are to calculate the percentage of the output*/
  private double head_perc,tail_perc,headtail_perc;
  
  /** This is a constructor which accepts input via object called from CoinSimViewer class*/
	CoinSimComponent(int trials)
	{
		CoinTossSimulator obj=new CoinTossSimulator(trials);
	   
		/**Save the variables*/
		
		 heads=obj.getTwoHeads();
		 tails=obj.getTwoTails();
		 headtails=obj.getHeadTails();
		
	     trialnum=obj.getNumTrials();
		
		 /**Calculate the percentage*/
	     
		 head_perc=((double)heads/trialnum)*100;
		 tail_perc=((double)tails/trialnum)*100;
		 headtail_perc=((double)headtails/trialnum)*100;
		
		 //Display the results in command prompt
		 obj.display();
		 
		 
		
	}
	@Override
	public void paintComponent(Graphics g)
	{   //Create an object of Graphics 2D and cast the original g too that 
		Graphics2D g2=(Graphics2D)g;
		//calculate the space of left (x coordinate) using getWidth() call and bottom coordinate through getHeight call
		 left=getWidth()/WIDTH_DIVIDER;
		 bottom=getHeight();
		 scale= (double)trialnum/(getHeight()-ADJUST_FACTOR);
		 
		 /**If the frame is too small for a fixed width to be displayed,
		  *  then this width itself has to be recalculated*/
		 if(width>left)
			 width=getWidth()/WIDTH_DIVIDER;
		 if(width<left)
			 width=SET_WIDTH;
		
		
		/**Create objects of bar class and pass appropriate variables which were calculated above.
		 * The major passing variable is heads,tails and headtails
		 *  which are the final output of CoinTossSimulator Prog*/
	   Bar bar=new Bar(bottom,left,width,heads,scale,Color.RED,"Heads: "+heads +"("+(int)head_perc+")");
	   bar.draw(g2); 
	  
	   Bar bar1=new Bar(bottom,2*left+width,width,tails,scale,Color.BLUE,"Tails: "+tails+"("+(int)tail_perc+")");
	   bar1.draw(g2);
		
	   Bar bar2=new Bar(bottom,3*left+2*width,width,headtails,scale,Color.GREEN,"HeadTails: "+headtails+"("+(int)headtail_perc+")");
	   bar2.draw(g2);
	   
	}
	
	
}
