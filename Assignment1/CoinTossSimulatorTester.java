// Name:Hemanth Eshwarappa
// USC NetID: 3185152842
// CS 455 PA1
// Fall 2017

public class CoinTossSimulatorTester {
    
    static int totalrun=0;
    public static final int MAX_VALUE= 2147483647;
	public static void main(String[] args) {
		//To test the Constructor
		CoinTossSimulator testerobject=new CoinTossSimulator();
		System.out.println("After Constructor");
		System.out.println("Number of trials[exp 0]");
		testerobject.display();
		if((testerobject.getHeadTails() + testerobject.getTwoHeads() + testerobject.getTwoTails()) == testerobject.getNumTrials())
		  System.out.println("Tosses add up Correctly? : " +"true");
		else
		  System.out.println("Tosses add up Correctly? : " + "false");
		
		System.out.println();
		
		
		// To test the values in run function call
		
		for(int trialnumber=0;trialnumber<5;trialnumber++)
		{	totalrun+=trialnumber;
			testerobject.run(trialnumber);
	    System.out.println("After trial number: run("+ trialnumber+")");		
		System.out.println("Number of trials[exp "+ totalrun +"]");
		testerobject.display();
		if((testerobject.getHeadTails() + testerobject.getTwoHeads() + testerobject.getTwoTails()) == testerobject.getNumTrials())
		   System.out.println("Tosses add up Correctly? : " +"true");
		else
		  System.out.println("Tosses add up Correctly? : " + "false");
		
		System.out.println();
		
		}
		// To test the values after reset.
		testerobject.reset();
		System.out.println("The object is reset now");
		totalrun=0;
	    System.out.println();
		
		for(int trialnumber=100000;trialnumber<100005;trialnumber++)
		{	totalrun+=trialnumber;
			testerobject.run(trialnumber);
	    System.out.println("After trial number: run("+ trialnumber+")");		
		System.out.println("Number of trials[exp "+ totalrun +"]");
		testerobject.display();
		if((testerobject.getHeadTails() + testerobject.getTwoHeads() + testerobject.getTwoTails()) == testerobject.getNumTrials())
		   System.out.println("Tosses add up Correctly? : " +"true");
		else
		  System.out.println("Tosses add up Correctly? : " + "false");
		
		System.out.println();
		
		}
		//To test for the largest value;
		testerobject.reset();
		System.out.println("The object is reset now");
		totalrun=0;
	    System.out.println();
	    
	    testerobject.run(MAX_VALUE);
	    testerobject.display();
	    if((testerobject.getHeadTails() + testerobject.getTwoHeads() + testerobject.getTwoTails()) == testerobject.getNumTrials())
			   System.out.println("Tosses add up Correctly? : " +"true");
			else
			  System.out.println("Tosses add up Correctly? : " + "false");
		totalrun=0;
		 System.out.println("*****************************THE END********************************************");
	}

	

}
