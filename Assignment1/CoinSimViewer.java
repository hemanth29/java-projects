// Name:Hemanth Eshwarappa
// USC NetID: 3185152842
// CS 455 PA1
// Fall 2017

import java.util.Scanner;

import javax.swing.JFrame;
public class CoinSimViewer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		while(true)
		{
		/**This creates a frame and then we set size of the frame*/	
		JFrame frame =new JFrame();
		
		frame.setSize(800,500);
		frame.setTitle("Toss Coin");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		/**Taking the input from the users*/
		System.out.println("Enter the Number of Trials:");
		System.out.println();
		Scanner input=new Scanner(System.in);
		int value=input.nextInt();
		if(value<0) System.out.println("ERROR: Number entered must be greater than 0");
		else
	     {
			//Creates the CoinSimComponent to draw over the frame
			CoinSimComponent output=new CoinSimComponent(value);
	     frame.add(output);
	     /**Setting the frame visible*/
	     frame.setVisible(true);}
		}
	}

}
