Initially your program will prompt for the number of trials to simulate (a trial is two coin tosses) on the console, error checking that a positive value is entered. (More details about error checking here.) This part of the program will be console-based, to keep things simpler (and faster if you are running it from aludra).
Then it will run the simulation and display a 500 tall by 800 wide pixel window with the results of that simulation. The results will consist of three labeled bars, each a different color, to show how many trials had the specified outcome. The label will show what the outcome was (e.g., Two Heads), the number of trials that had that result, and the percentage of trials that had that result (rounded to the nearest one percent). Because the simulation uses random coin tosses (simulated using a random-number generator) subsequent runs with the same input will produce different results.

Here is a screen-shot of output from one run of our solution to this assignment, where we do 1000 trials:


![picture](http://scf.usc.edu/~csci455/curr/assgts/pa1/1000TrialsFull.png)

Remember, your output will not be identical to this because of the random nature of the results.

Note the placement of each of the bars evenly across the window. In addition, the height of each bar is given so that 100% would fill up most of the height of the window (but not run into the top of it). Thus the 48% of trials that resulted in a head and a tail in the example above fills up roughly half of the height of the window.

Also, your bar graph should get resized appropriately if the window gets resized. As mentioned in the textbook, every time a window gets resized or iconified and de-iconified paintComponent gets called again. Here's a later screen-shot created during same run shown above, but after the window had been resized:
![picture](http://scf.usc.edu/~csci455/curr/assgts/pa1/1000TrialsResized.png)