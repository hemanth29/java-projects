Name: Hemanth Eshwarappa
USC NetId:eshwarap
CSCI 455 PA1
Fall 2017

----------------------------------------------
CERTIFY IT'S YOUR WORK

"I certify that the work submitted for this assignment does not
violate USC's student conduct code.  In particular, the work is my
own, not a collaboration, and does not involve code created by other
people, except for the the resources explicitly mentioned in the CS 455
Course Syllabus.  And I did not share my solution or parts of it with
other students in the course."

Initial below to "sign" the above statement:


---Hemanth Eshwarappa-------------------------------------------
ACKNOWLEDGE ANY OUTSIDE SOURCES

The implementation for the Bar class and CoinTossSimulator class were given in the asssigment.

The Scale was calculated from the two built in functions 
getHeight and getWeight and the idea to scale it in that way was taken from the text.


----------------------------------------------
KNOWN BUGS or LIMITATIONS:

The max value of int is known and the program only works within that limit.

----------------------------------------------
ANY OTHER NOTES FOR THE GRADER:

I have taken a constructor with parameter as well in CoinTossSimulator by which I can directly pass the number of trials instead of calling the run function.


----------------------------------------------
ANSWERS TO ASSIGNMENT README QUESTIONS (if applicable):

1. As the number of trials increase , the probability of head tails goes to nearly 50 percent and the other two to twenty five percent.

2. As I increased the trials from 1 to 100 I got varied results in the graph as well. But after 100 to 200, it was mostly within the 25,25 and 50 probability but after 400 and 500, it saturated to 1 percent of those probabilities of
heads=25 percent
tails=25 percent
head tails=50 percent

3. We can run the program upto the max int value and It works for that max value as well. As soon as it exceeds the max int value,it overflows.



